from typing import List, Dict

import boto3
import botocore.exceptions
import configparser
import os
import sys
import argparse

# Шаблон HTML документа "Индексная страница"
INDEX_PAGE_TEMPLATE = """
<!doctype html>
<meta charset="UTF-8">
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.css" />
        <title>Фотоархив</title>
    </head>
    <body>
        <h1><p>Фотоархив<p></h1>
        <ul>
            {album_links}
        </ul>
    </body>
</html>
"""

# Шаблон HTML документа "Страница альбома"
ALBUM_PAGE_TEMPLATE = """
<!doctype html>
<meta charset="UTF-8">
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.css" />
        <style>
            .galleria{{ width: 960px; height: 540px; background: #000 }}
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/galleria.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.js"></script>
    </head>
    <body>
        <div class="galleria">
            {photo_tags}
        </div>
        <p>Вернуться на <a href="index.html">главную страницу</a> фотоархива</p>
        <script>
            (function() {{
                Galleria.run('.galleria');
            }}());
        </script>
    </body>
</html>
"""

# Шаблон HTML документа "Страница ошибки"
ERROR_PAGE_TEMPLATE = """
<!doctype html>
<meta charset="UTF-8">
<html>
    <head>
        <title>Фотоархив</title>
    </head>
    <body>
        <h1>Ошибка</h1>
        <p>Ошибка при доступе к фотоархиву. Вернитесь на <a href="index.html">главную страницу</a> фотоархива.</p>
    </body>
</html>
"""

parser = argparse.ArgumentParser(prog='cloudphoto', description='Manage cloud photo albums')

subparsers = parser.add_subparsers(dest='command', metavar='command')
subparsers.required = True

list_parser = subparsers.add_parser('list', help='List all albums')

upload_parser = subparsers.add_parser('upload', help='Upload photos')
upload_parser.add_argument('--album', help='Album name')
upload_parser.add_argument('--path', help='Photos directory', default='.')

delete_parser = subparsers.add_parser('delete', help='Delete an album')
delete_parser.add_argument('--album', help='Album name')

mksite_parser = subparsers.add_parser('mksite', help='Generate website')
mksite_parser.add_argument('--bucket', help='Bucket name')

init_parser = subparsers.add_parser('init', help='Initialize program')


def get_config_file_path() -> str:
    """
    Возвращает путь к конфигурационному файлу.

    Returns:
        str: Путь к конфигурационному файлу.
    """
    config_dir = os.path.expanduser("~/.config/cloudphoto")
    os.makedirs(config_dir, exist_ok=True)
    return os.path.join(config_dir, "cloudphotorc.ini")


def generate_error_page():
    """
    Генерирует и сохраняет страницу ошибки.
    """

    error_page = ERROR_PAGE_TEMPLATE

    return error_page


def check_config_file() -> Dict[str, Dict[str, str]]:
    """
    Проверяет наличие и заполненность конфигурационного файла cloudphotorc.

    Returns:
        Dict[str, Dict[str, str]]: Конфигурация из файла cloudphotorc.
    """
    config_file = get_config_file_path()

    if not os.path.exists(config_file):
        print("Configuration file not found: cloudphotorc")
        sys.exit(1)

    config = configparser.ConfigParser()
    config.read(config_file)

    required_fields = ["bucket", "aws_access_key_id", "aws_secret_access_key", "region", "endpoint_url"]
    for field in required_fields:
        if not config.has_option("DEFAULT", field) or not config.get("DEFAULT", field):
            print(f"Configuration file is missing or incomplete '{field}' field")
            sys.exit(1)

    return config


def get_bucket_name() -> str:
    """
    Получает название бакета из конфигурационного файла.

    Returns:
        str: Название бакета.
    """
    config = check_config_file()
    return config.get('DEFAULT', 'bucket')


def get_albums():
    """
    Возвращает список альбомов в облачном хранилище.

    Returns:
        list: Список альбомов.
    """
    config = check_config_file()
    s3 = boto3.resource('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                        aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                        endpoint_url=config["DEFAULT"]["endpoint_url"])
    bucket_name = get_bucket_name()
    albums = set()

    try:
        bucket = s3.Bucket(bucket_name)
        for obj in bucket.objects.all():
            album_name = os.path.dirname(obj.key)
            albums.add(album_name)
    except Exception as e:
        print(f"Failed to get albums: {str(e)}")

    return list(albums)[1:]


def create_bucket(bucket_name):
    """
    Создает бакет в облачном хранилище.

    Args:
        bucket_name (str): Название бакета.

    Returns:
        bool: True, если бакет успешно создан, False в противном случае.
    """
    config = check_config_file()
    s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                      aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                      endpoint_url=config["DEFAULT"]["endpoint_url"])
    try:
        s3.create_bucket(Bucket=bucket_name)
        return True
    except Exception as e:
        print(f"Failed to create bucket '{bucket_name}': {str(e)}")
        return False


def list_albums():
    """
    Выводит список альбомов в облачном хранилище.
    """
    config = check_config_file()
    s3 = boto3.resource('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                        aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                        endpoint_url=config["DEFAULT"]["endpoint_url"])
    bucket_name = get_bucket_name()
    try:
        bucket = s3.Bucket(bucket_name)
        albums = set()

        for obj in bucket.objects.all():
            album_name = os.path.dirname(obj.key)
            if album_name:
                albums.add(album_name)

        if albums:
            for album in sorted(albums):
                print(album)
        else:
            print("Photo albums not found", file=sys.stderr)
    except Exception as e:
        print(f"Failed to list albums: {str(e)}", file=sys.stderr)


def upload_photos(album, photos_dir):
    """
    Отправляет фотографии в облачное хранилище.

    Args:
        album (str): Название фотоальбома.
        photos_dir (str): Путь к каталогу с фотографиями.
    """
    if not os.path.exists(photos_dir) or not os.path.isdir(photos_dir):
        print(f"Warning: No such directory '{photos_dir}'", file=sys.stderr)
        sys.exit(1)

    config = check_config_file()
    s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                      aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                      endpoint_url=config["DEFAULT"]["endpoint_url"])
    bucket_name = get_bucket_name()

    # Проверка существования фотоальбома
    try:
        response = s3.head_object(Bucket=bucket_name, Key=f"{album}/")
        album_exists = True
    except:
        album_exists = False

    # Создание фотоальбома, если не существует
    if not album_exists:
        try:
            s3.put_object(Bucket=bucket_name, Key=f"{album}/")
            print(f"Creating album '{album}'...")
        except Exception as e:
            print(f"Failed to create album '{album}': {str(e)}", file=sys.stderr)
            sys.exit(1)

    # Получение списка фотографий
    photo_files = []
    for file_name in os.listdir(photos_dir):
        if file_name.lower().endswith('.jpg') or file_name.lower().endswith('.jpeg'):
            photo_files.append(file_name)

    if not photo_files:
        print(f"Warning: Photos not found in directory '{photos_dir}'", file=sys.stderr)
        sys.exit(1)

    # Отправка фотографий в облачное хранилище
    for photo_file in photo_files:
        photo_path = os.path.join(photos_dir, photo_file)
        try:
            s3.upload_file(photo_path, bucket_name, f"{album}/{photo_file}")
        except Exception as e:
            print(f"Warning: Photo not sent {photo_file}: {str(e)}")

    sys.exit(0)


def delete_album(album):
    """
    Удаляет альбом и все фотографии в нем из облачного хранилища.

    Args:
        album (str): Название фотоальбома.
    """
    config = check_config_file()
    s3 = boto3.resource('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                        aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                        endpoint_url=config["DEFAULT"]["endpoint_url"])
    bucket_name = get_bucket_name()

    objects_to_delete = [
        obj.key for obj in s3.Bucket(bucket_name).objects.filter(Prefix=f"{album}/")
    ]
    if album not in get_albums():
        print(f"Warning: Photo album not found '{album}'", file=sys.stderr)
        sys.exit(1)

    # Удаляем фотографии в альбоме
    if objects_to_delete:
        s3.Bucket(bucket_name).delete_objects(
            Delete={
                'Objects': [{'Key': obj_key} for obj_key in objects_to_delete]
            }
        )

    # Удаляем определение альбома
    try:
        s3.Object(bucket_name, f"{album}/").delete()
        print(f"Album '{album}' deleted successfully.")
        sys.exit(0)
    except botocore.exceptions.ClientError as e:
        print(f"Warning: Photo album not found '{album}': {str(e)}", file=sys.stderr)
        sys.exit(1)


def generate_photo_tags(album_photos: List[str], album_name: str) -> str:
    """
    Генерирует теги <img> для фотографий в альбоме.

    Args:
        album_photos (List[str]): Список имен фотографий.
        album_name (str): Имя альбома.
    Returns:
        str: Строка с тегами <img> для фотографий.
    """
    photo_tags = ""
    for photo in album_photos:
        photo_key = f"{album_name}/{photo}"

        # Возвращаем ссылку на фотографию в облачном хранилище
        config = check_config_file()
        s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                          aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                          endpoint_url=config["DEFAULT"]["endpoint_url"])
        bucket_name = get_bucket_name()
        photo_url = s3.generate_presigned_url(
            "get_object",
            Params={"Bucket": bucket_name, "Key": photo_key}
        )
        photo_tags += f'<img src="{photo_url}" data-title="{photo}">\n'
    return photo_tags


def get_album_photos(album_name: str) -> List[str]:
    """
    Получает список фотографий из альбома.

    Args:
        album_name (str): Имя альбома.

    Returns:
        List[str]: Список имен фотографий из альбома.
    """
    # Получаем список объектов в бакете, соответствующих альбому
    config = check_config_file()
    s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                      aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                      endpoint_url=config["DEFAULT"]["endpoint_url"])
    bucket_name = get_bucket_name()
    album_folder = f"{album_name}/"
    try:
        response = s3.list_objects_v2(Bucket=bucket_name, Prefix=album_folder)
        if "Contents" in response:
            # Извлекаем имена фотографий из списка объектов
            album_photos = [
                obj["Key"].replace(album_folder, "") for obj in response["Contents"]
            ]
            return album_photos[1:]
    except Exception as e:
        print(f"Error retrieving album {album_name} photos: {str(e)}", file=sys.stderr)
        sys.exit(1)
    return []


def generate_album_page(album_name: str, album_number: int):
    """
    Генерирует и сохраняет страницу альбома.

    Args:
        album_number (int): Номер альбома.
        album_name (str): Имя альбома.
    """
    album_photos = get_album_photos(album_name)

    photo_tags = generate_photo_tags(album_photos, album_name)

    album_page = ALBUM_PAGE_TEMPLATE.format(photo_tags=photo_tags)

    album_key = f"album{album_number}.html"
    config = check_config_file()
    s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                      aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                      endpoint_url=config["DEFAULT"]["endpoint_url"])

    try:
        s3.put_object(Body=album_page, Bucket=get_bucket_name(), Key=album_key)
    except Exception as e:
        print(f"Failed to generate album page: {str(e)}", file=sys.stderr)


def generate_index_page():
    """
    Генерирует и сохраняет индексную страницу.
    """

    albums = get_albums()
    album_links = ""
    for album_number, album_name in enumerate(albums, start=1):
        album_links += f"<li><a href='album{album_number}.html'>{album_name}</a></li>\n"

    index_page = INDEX_PAGE_TEMPLATE.format(album_links=album_links)
    return index_page


def bucket_exists(bucket_name):
    """
    Проверяет, существует ли бакет в облачном хранилище.

    Args:
        bucket_name (str): Название бакета.

    Returns:
        bool: True, если бакет существует, False в противном случае.
    """
    config = check_config_file()
    s3 = boto3.client('s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                      aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                      endpoint_url=config["DEFAULT"]["endpoint_url"])
    try:
        response = s3.head_bucket(Bucket=bucket_name)
        return True
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            return False
        else:
            print(f"Failed to check bucket existence: {str(e)}")
            return False
    except Exception as e:
        print(f"Failed to check bucket existence: {str(e)}")
        return False


def generate_error_page():
    """
    Генерирует и сохраняет страницу ошибки.
    """

    error_page = ERROR_PAGE_TEMPLATE

    return error_page


def init_program():
    """
    Инициализирует программу.
    """
    config = configparser.ConfigParser()

    # Получение значений от пользователя
    aws_access_key_id = input("Введите идентификатор секретного статического ключа: ")
    aws_secret_access_key = input("Введите секретный статический ключ: ")
    bucket_name = input("Введите название бакета: ")

    # Сохранение значений в конфигурационный файл
    config['DEFAULT'] = {
        'aws_access_key_id': aws_access_key_id,
        'aws_secret_access_key': aws_secret_access_key,
        'bucket': bucket_name,
        'region': 'ru-central1',
        'endpoint_url': 'https://storage.yandexcloud.net'
    }
    with open(get_config_file_path(), 'w') as configfile:
        config.write(configfile)

    # Создание бакета, если он не существует
    if not bucket_exists(bucket_name):
        if create_bucket(bucket_name):
            print(f"Bucket '{bucket_name}' created successfully")
        else:
            print(f"Failed to create bucket '{bucket_name}'")


def make_site():
    index_page = generate_index_page()
    error_page = generate_error_page()

    config = check_config_file()
    s3 = boto3.session.Session(profile_name='admin')
    s3_pub_resource = s3.resource(service_name='s3', aws_access_key_id=config["DEFAULT"]["aws_access_key_id"],
                                  aws_secret_access_key=config["DEFAULT"]["aws_secret_access_key"],
                                  endpoint_url=config["DEFAULT"]["endpoint_url"])
    s3_pub_bucket = s3_pub_resource.Bucket(get_bucket_name())
    s3_pub_bucket.Acl().put(ACL='public-read')
    bucket_website = s3_pub_bucket.Website()
    index_document = {'Suffix': 'index.html'}
    error_document = {'Key': 'error.html'}
    bucket_website.put(WebsiteConfiguration={'ErrorDocument': error_document, 'IndexDocument': index_document})

    html_object = s3_pub_bucket.Object('index.html')
    html_object.put(Body=index_page, ContentType='text/html')
    html_object = s3_pub_bucket.Object('error.html')
    html_object.put(Body=error_page, ContentType='text/html')

    albums = get_albums()
    for album_number, name in enumerate(albums):
        generate_album_page(name, album_number + 1)

    bucket_name = get_bucket_name()
    print(f"https://{bucket_name}.website.yandexcloud.net/")


def unknown_command(command: str):
    """
    Обрабатывает неизвестную команду.

    Args:
        command (str): Неизвестная команда.
    """
    print(f"Unknown command: {command}", file=sys.stderr)
    sys.exit(1)


def main():
    args = parser.parse_args()

    match args.command:
        case "list":
            list_albums()
        case "upload":
            upload_photos(args.album, args.path)
        case "delete":
            delete_album(args.album)
        case "mksite":
            make_site()
        case "init":
            init_program()
        case _:
            unknown_command(args.command)


if __name__ == "__main__":
    main()
